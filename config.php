<?php
/**
 * Created by JetBrains PhpStorm.
 * User: neolf
 * Date: 14-2-26
 * Time: 下午4:48
 * To change this template use File | Settings | File Templates.
 */

define(DIR_PREFIX,'');
define(SCENICID,237);

function getJson ($url) {
    $ch = curl_init ($url) ;
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1) ;
    $res = curl_exec ($ch) ;
    curl_close ($ch) ;
    return $res ;
}

function getScenic ($scenicId){
    $scenicJson = getJson("http://www.3dyou.cn/api/scenic/".$scenicId) ;
    $data = json_decode($scenicJson,True);
    $scenic = $data["data"];
    return $scenic;
}

function listdir($start_dir='.') {

    $files = array();
    if (is_dir($start_dir)) {
        $fh = opendir($start_dir);
        while (($file = readdir($fh)) !== false) {
            # loop through the files, skipping . and .., and recursing if necessary
            if (strcmp($file, '.')==0 || strcmp($file, '..')==0 || strcmp($file, 'Thumb.db')==0) continue;
            $filepath = $start_dir . '/' . $file;
            if ( is_dir($filepath) )
                $files = array_merge($files, listdir($filepath));
            else
                array_push($files, $filepath);
        }
        closedir($fh);
    } else {
        # false if the function was called with an invalid non-directory argument
        $files = false;
    }
    return $files;
}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

