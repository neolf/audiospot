<?php
/**
 * Created by JetBrains PhpStorm.
 * User: neolf
 * Date: 14-2-26
 * Time: 下午4:48
 * To change this template use File | Settings | File Templates.
 */

include_once "config.php";


$BASEHOST = dirname(curPageURL());

$scenic = getScenic(SCENICID);
$spots = $scenic['spots'];

$data = array();
foreach ($spots as $spot) {
    $files = listdir(DIR_PREFIX.$spot['id']);
    foreach ($files as &$file) {
        $file = $BASEHOST."/".$file;
    }
    $data[$spot['id']] = $files;
}

echo json_encode($data);





