
var infoUrl="http://www.3dyou.cn/api/scenic/"+scenicID;

var extInfoUrl="json.php";

var map,scenic;

var extAudio = {};


function MyfunctionHandle(event) {  console.log("scenicload :",event);}


function initMap(){
    map = new OE.Map('map',{locate:true,closePopupOnClick:false});
    getInfo(infoUrl);
}





function getInfo(url1){
    OE.Ajax.request(url1,{
        onSuccess:function(resp){
            var json = JSON.parse(resp.response);
            scenic = OE.Scenic.create(json.data);
            map.load(scenic);
            initEvent();
			showAllSpot();
			getExtInfo(extInfoUrl);
        }
    });
}


function getExtInfo(url){
    OE.Ajax.request(url,{
        onSuccess:function(resp){
            var extinfos = JSON.parse(resp.response);
            console.log(extinfos);
			for(var key in extinfos){
				if(extinfos[key]){
					extAudio[key] = extinfos[key][0];
				}				
			}
			console.log(extAudio);
            setAudioSpotPopupContent();
        }
    });
}

function setAudioSpotPopupContent(){
    var spots = scenic.selectAllSpot();
    for(var k in spots){
        var spot = spots[k];
        var audio = extAudio[spot.id];
        spot.setContent(spot.id+"<strong>"+spot.name+"</strong>"+"<br/>"+'<audio src="'+audio+'" controls="controls" preload="auto"></audio>');
    }
}




function reloadMap(){
    map.reload(); //重新载入地图
}

function destroyMap(){
    map.destroy();
    $('.btn').hide();
    $('#initBtn').show();
}


function doEnlarge(){
    map.zoomIn();
}

function doSmall(){
    
    map.zoomOut();
}



function showAllSpot(){
    map.load(scenic.selectAllSpot()); //显示全部
    $('#showAllBtn').html('隐藏所有标注点').attr('onClick','hideAllSpot();');
}


/*** event ***/
function initEvent(){
    map.on('zoom',function(event) {
        console.log("map zoom :",event);
    });
    map.on('move',function(event) {
        console.log("map move :",event);
    });

    map.on('click',function(event) {
        console.log("map click :",event);
		map.closePopup();
    });

    map.on("scenicload",function(event) {
        console.log("scenicload :",event);
    });
    map.on("scenicloaded",function(event) {
        console.log("scenicloaded :",event);
    });

    map.on("locationerror",function(event) {
        console.log("locationerror :",event);
    });
    map.on("locationfound",function(event) {
        console.log("locationfound :",event);
    });

    map.on("clickSpot",function(event) {
        console.log("clickSpot :",event);
        //下面可以在标注点弹出时修改其内容
        // event.setContent(event.id+"<strong>"+event.name+"</strong>"+"<br/>最后更新时间:"+Date.now());
    });
}

