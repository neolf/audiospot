微景地图富媒体扩展
=======================

如何在微景地图中加入音频信息？

在自己的空间创建一个目录放上自己的资源，利用微景地图提供的api构建自己的微景地图。


配置
===========
修改config.php中的景区id

define(SCENICID,**237**);



生成目录
============

运行目录下的init.php，会自动生成相应景点目录  


放置资源
============
在相关目录下放置好资源，
读取 json.php
JSON


    10359
        "http://aaa.3dyou.cn/lf/advencedSpot/10359/2.mp3"
    10362
        "http://aaa.3dyou.cn/lf/advencedSpot/10362/1.mp3"
    10361
        "http://aaa.3dyou.cn/lf/advencedSpot/10361/Sleep Away.mp3"
    10364
        "http://aaa.3dyou.cn/lf/advencedSpot/10364/3.mp3"
    10363
        "http://aaa.3dyou.cn/lf/advencedSpot/10363/Maid with the Flaxen Hair.mp3"
    24452
        "http://aaa.3dyou.cn/lf/advencedSpot/24452/3.mp3"



例子
============

http://aaa.3dyou.cn/lf/advencedSpot/example.php



高级功能
============

缓存神马的。。。待续


Have Fun~